#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from tkinter import *
from tkinter.filedialog import askopenfilename #pour les gestions de fichiers
import subprocess #pour pouvoir lancer HMMscan et HMMsearch
import os #permet d'utiliser commandes bash
from ftplib import FTP #permet de vérifier si la librairie Pfam-A.hmm existe


##PARTIE 1 - Interface graphique pour la recherche de domaines Pfam

class AppPartie1:

    def __init__(self):
        self.window = Tk()
        self.window.title("Hmmscan /Hmmsearch luncher")
        self.window.geometry("1000x500")
        self.window.minsize(480, 360)
        self.window.config(background='steelblue4')

        # initialization des composants
        self.frame = Frame(self.window, bg='steelblue4')
        self.frames = [PhotoImage(file='load2.gif',format = 'gif -index %i' %(i)) for i in range(100)] #La classe Tkinter PhotoImage
        #reconnaît les images GIF. Un GIF contient une succession d'images
        self.label = Label(self.frame,bg='steelblue4') #le gif sera intégré dans ce label

        # creation des composants
        self.create_widgets()

        # empaquetage
        self.frame.pack(expand=YES)
        self.label.pack(side=BOTTOM)

    def create_widgets(self):
        self.create_title()
        self.create_subtitle()
        self.create_fasta_button()
        self.create_hmm_button()
        self.hmmscan()
        self.hmmsearch()

    def create_title(self):
        label_title = Label(self.frame, text="Graphic interface HmmSearch / HmmScan", font=("Courrier", 40), bg='steelblue4',
                            fg='white')
        label_title.pack(padx=5,pady=5)

    def create_subtitle(self):
        label_subtitle = Label(self.frame, text="Launch a job", font=("Courrier", 25), bg='steelblue4',
                               fg='white')
        label_subtitle.pack(padx=5,pady=5)

    def create_fasta_button(self):
        fasta_button = Button(self.frame, text="fasta file", font=("Courrier", 25), bg='white', fg='steelblue4',
                           command=self.open_dir_file)
        fasta_button.pack(pady=25, fill=X)

    def create_hmm_button(self):
        hmm_button = Button(self.frame, text="Hmm prolfils", font=("Courrier", 25), bg='white', fg='steelblue4',
                           command=self.open_file_HMM)
        hmm_button.pack(pady=25, fill=X)

    def open_dir_file(self):
        self.chemin=askopenfilename(initialdir =  "C:\Desktop", title = "Select A File",
                                              filetypes =(("Fasta File", "*.faa"),("Fasta File","*.fasta")) ) #permet de récupérer le fichier
        #fasta de l'utilisateur pour pouvoir lancer un HMMscan ou HMMsearch


    def stockage_chemin(self):
        return self.chemin


    def open_file_HMM(self):
        self.file_HMM=askopenfilename(initialdir =  "C:\Desktop", title = "Select A File",
                                              filetypes =(("Fasta File", "*.hmm"),("Fasta File","*.txt")) )


    def stockage_chemin_HMM(self):
        return self.file_HMM

    def hmmsearch(self):
        hscan_button = Button(self.frame, text="HMMsearch", font=("Courrier", 25), bg='white', fg='steelblue4',
                              command=self.loadhmmsearch)
	hsearch_button.pack(padx=5,pady=5,side=LEFT)

    def lance_hmmsearch(self):

        subprocess.call(["hmmsearch", "--domtblout", "hmmsearch_out.txt", self.file_HMM,self.chemin])

        ##permet de lancer le hmmsearch


    def lance_hmmscan(self):

        os.system('rm *.h3[ifmp]') #on supprime les 4 fichiers compressés binaires pour pouvoir refaire le hmmpress sinon message d'erreur

        subprocess.call(["hmmpress",self.file_HMM]) #on compresse 4 fichiers compressés binaires
        subprocess.call(["hmmscan", "--domtblout","hmmscan_out.txt",self.file_HMM, self.chemin]) #on lance le hmmscan



    def loadhmmsearch(self):
        ##lors de l'appui sur le bouton HMMsearch, renvoie à cette fonction qui permet de lancer un gif load, afin que l'utilisateur puisse patienter
        #le temps que le processus se finisse et permet de lancer le processus HMMsearch
        self.window.after(0, self.load_gif,0) #after appelle la fonction load_gif sans attente
        self.lance_hmmsearch()

    def loadhmmscan(self):
        ##lors de l'appui sur le bouton HMMscan, renvoie à cette fonction qui permet de lancer un gif load, afin que l'utilisateur puisse patienter
        #le temps que le processus se finisse et permet de lancer le processus HMMscan
        self.window.after(0, self.load_gif,0)
        self.lance_hmmscan()

    def load_gif(self,ind):

        self.ind=ind
        frame = self.frames[self.ind] #l'indice va de 0 à 99, donc ce frame comporte 100 images
        self.ind += 1 #on incrémente l'indice de 1
        self.label.configure(compound="bottom",text="Veuillez patientez",image=frame) #on affiche image par image le gif dans un label
        self.window.after(100, self.load_gif, self.ind) #on actualise la zone graphique toutes les 100 ms


    def verif_librairie(self):
        ftp = FTP('ftp.ebi.ac.uk')
        file_name = 'pub/databases/Pfam/current_release/Pfam-A.hmm.gz'
        if file_name in ftp.nlst():
            #print("File exists")
            pass
        else:
            #print("echec")
            label_informatif = Label(self.frame, text="Attention la librairie Pfam-A.hmm is no longer available ", font=("Courrier", 40), bg='steelblue4',
                            fg='white')



app = AppPartie1()
#permet d'afficher l'interface graphique
app.window.mainloop()