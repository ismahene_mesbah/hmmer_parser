try
{
        //Création d'une instance PDO représentant une connexion à Mysql
        $db = new PDO("mysql:host=".$_SERVER['dbHost']."; dbname=".$_SERVER['dbBd'], $_SERVER['dbLogin'], $_SERVER['dbPass']);

        //Configuration du pilote : nous voulons recevoir les messages d'erreurs en cas de requêtes mal formées

        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(Exception $e)
{
         echo "<pre>Échec : " . $e->getMessage()."<pre>";
}

?>