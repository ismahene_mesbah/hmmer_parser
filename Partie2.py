#!/usr/bin/env python3
# coding: utf-8

import tkinter
from tkinter import *
from tkinter.filedialog import askopenfilename  #pour ouvrir fenetre qui permet d 'entrer le fichier
from tkinter import ttk
import pymysql # permet de manipuler bases de données
from Bio import SearchIO  #permet de parser fichier
import cnx_insertion as mr #librairie que nous avons creer : elle contient une fonction de cnx et une fonction d'insertion



mr.connexion() #se connecter à la bdd sql

class Parser:

    def __init__(self):

        self.window = Tk()    #crée fenêtre tkinter
        self.var2=IntVar()   #initialisation de la variable de la pvalue
        self.var_scan=IntVar()
        self.var_search = IntVar()  #initialisation de la variable du radiobutton
        self.window.title("Parse HMMer file")   #titre de la fenêtre
        self.window.geometry("1000x500")     #taille de la fenêtre
        self.window.config(background='steelblue4')  #configuration de la couleur de la fenêtre

        # initialization des composants
        self.frame = Frame(self.window, bg='steelblue4')   #frame principal
        self.inner_frame=Frame(self.frame,bg='steelblue4') #sous frame dans le frame principal

        #creation des noms des colonnes
        self.colnames=('Target name', 'Query name', 'e value', 'Domain start', 'Domain end','Target length','coverage')

        #création d'une barre déroulante
        self.s=Scrollbar(self.inner_frame)

        #creation du treeview qui permet de visualiser notre fichier après parsing
        self.tab = ttk.Treeview(self.inner_frame,columns=self.colnames,selectmode="extended",yscrollcommand=self.s.set)

        #défénition du header et de la taille de chaque colonne
        self.tab.heading("Target name", text="Target name")
        self.tab.column("Target name",minwidth=0,width=100, stretch=NO)
        self.tab.heading("Query name", text="Query name")
        self.tab.column("Query name",minwidth=0,width=100, stretch=NO)
        self.tab.heading("e value", text="e value")
        self.tab.column("e value",minwidth=0,width=100, stretch=NO)
        self.tab.heading("Domain start", text="Start domain")
        self.tab.column("Domain start",minwidth=0,width=100, stretch=NO)
        self.tab.heading("Domain end", text="Domain end")
        self.tab.column("Domain end",minwidth=0,width=100, stretch=NO)
        self.tab.heading("Target length", text="Target length")
        self.tab.column("Target length",minwidth=0,width=100, stretch=NO)
        self.tab.heading("coverage", text="coverage")
        self.tab.column("coverage",minwidth=0,width=100, stretch=NO)

        #fixer la barre déroulante (scrollbar) sur la coordonnée y
        self.s.config(command=self.tab.yview)

        # creation des composants
        self.create_widgets()

        # empaquetage
        self.frame.pack(expand=YES)
        self.inner_frame.pack(side=BOTTOM)


    def create_widgets(self):
        self.create_title()
        self.create_hmm_button()
        self.hmm_button()
        self.export_button()
        self.create_textbox()
        self.retrieve_input()
        self.radio_button()




    def create_title(self):   #creation du titre
        label_title = Label(self.frame, text="Graphical interface for parsing HMMer files", font=("Courrier", 40), bg='steelblue4',
                            fg='white')
        label_title.pack(padx=5,pady=5,side=TOP)



    def create_hmm_button(self): #creation du bouton qui permet de télécharger le fichier à parser
        button = Button(self.frame, text="Download Hmmer file", font=("Courrier", 25), bg='white', fg='navy',
                           command=self.fileDialog)
        button.pack(pady=25, fill=X)
    def export_button(self): #bouton qui permet d 'insérer les données dans la base de données
        ex_button = Button(self.frame, text="Export to database" , command=self.get_value)
        ex_button.pack(side=RIGHT,anchor=W)

    def fileDialog(self): #fonction qui permet d'uploader que des fichier text
        self.chemin=askopenfilename(initialdir =  "/", title = "Select A File",
                                              filetypes =(("Text File", "*.txt"),("all files","*.*")) )


    def stockage_chemin(self):
        return self.chemin


    def radio_button(self):
        R1 = Radiobutton(self.frame, text="Hmmscan", bg='steelblue4',fg='white',variable=self.var_scan, value=1)
        R1.pack(anchor=W)

        R2 = Radiobutton(self.frame, text="Hmmsearch",bg='steelblue4',fg='white', variable=self.var_search, value=2)
        R2.pack( anchor=W )

    def create_textbox(self):
        L1=Label(self.frame, text="e-value") #creation du label e-value
        L1.pack(padx=5,pady=5,side=LEFT)
        self.pvalue=Entry(self.frame,bg='white', fg='navy',textvariable=self.var2) #creation champs de remplissage de e value
        self.pvalue.pack(padx=5,pady=5,side=LEFT)
        buttonCommit=Button(self.frame, text="apply filter",  command=self.filter_by_evalue).pack(side=LEFT) #bouton filtrer

    def retrieve_input(self):

        return self.var2.get() #récupérer valeur rentrée





    def hmm_button(self): #bouton qui fait appel à la fonction du parsing du fichier
        hsearch_button = Button(self.frame, text="Parse file", font=("Courrier", 25), bg='white', fg='navy',
                                command=self.hmmer_parser)
        hsearch_button.pack(padx=25,pady=5)

    def hmmer_parser(self): #fonction qui parse fichier output
        self.coverage={} # creation d'un dictionnaire qui contiendra les valeurs du recouvrement
        for qresult in SearchIO.parse(self.chemin, 'hmmscan3-domtab'): #utilisation du module parse  de la librairie SearchIO.
            self.query_id = qresult.id  #query name
            self.query_len = qresult.seq_len   #query length
            self.hits = qresult.hits   #accéder à la classe hits qui va permettre de récupérer toutes les infos sur les hits
            self.hsps = qresult.hsps #accéde à la classe hsps
            self.num_hits = len(self.hits)   #récupérer longueurs des hits (donc récupérer nb lignes de fichier)
            if self.num_hits > 0: #si fichier contient des hits (homologues)
                for i in range(0,self.num_hits): #boucler sur nb hits
                    self.hit_evalue = self.hits[i].evalue #evalue
                    self.target_name = self.hits[i].id #target name
                    self.tlength = self.hits[i].seq_len #target length
                    self.hmm_start = self.hsps[i].hit_start #début profil hmm
                    self.domain_start=self.hsps[i].env_start #début domaine
                    self.domain_end=self.hsps[i].env_end  #fin domaine (enveloppe)
                    self.hmm_end= self.hsps[i].hit_end #fin profil hmm
                    length=self.hmm_end-self.hmm_start #calculer longueur du profil hmm
                    self.alig_start = self.hsps[i].hit_start #début profil hmm
                    cov=(length/self.tlength)*100     #pourcentage coverage
                    cov=round(cov, 2)       #arrondir recouvrement (deux chiffre apres la virgule)
                    self.coverage={"coverage":cov }  #remplir dictionnaire
                    #ici on insère tous ce qu'on a récupérer dans notre tableau
                    self.tab.insert('',i,values=(self.target_name,self.query_id,self.hit_evalue,self.domain_start,self.domain_end,self.tlength,cov))
                    if  self.var2.get()==0: #si l'utilisateur n'a pas fixé de e-value
                        self.tab.grid(row=0, column=0, sticky=N+S+E+W)   #on affiche le tableau rempli

                        self.s.grid(row=0, column=1, sticky="ns") #on affiche aussi la barre déroulante

                    else:
                        print("oops! an error has occured, please upload your file again") #erreur


    def filter_by_evalue(self):
        self.tab.delete(*self.tab.get_children()) #supprimer le tableau précédent qui contient l'intégralité du fichier
        self.dico={}   #crée dictionnaire
        self.coverage={} #crée dictionnaire recouvrement
        seuil=float(self.pvalue.get())  #on récupére la p value et on force sa conversion en float



        for qresult in SearchIO.parse(self.chemin, 'hmmscan3-domtab'): #parser avc module parse de librairie SearchIO
            self.query_id = qresult.id
            self.hsps = qresult.hsps
            self.evalue = qresult[0].evalue #récupérer toutes les e value
            self.dico= {'Evalue' : self.evalue} #remplir dictionnaire avc les e value

            length=len(self.dico) #récuprérer longueur dictionnaire des e value
            for i in range(0,self.num_hits): #boucler sur totalité des hits
                self.target_name = self.hits[i].id
                self.tlength = self.hits[i].seq_len
                self.hmm_start = self.hsps[i].hit_start
                self.hmm_end= self.hsps[i].hit_end
                self.domain_start=self.hsps[i].env_start
                self.domain_end=self.hsps[i].env_end
                length=self.hmm_end-self.hmm_start
                cov=(length/self.tlength)*100     #pourcentage coverage
                cov=round(cov, 2)
                self.coverage={"coverage":cov }
                for i in self.dico.values():  #parcourir dictionnaire et comparer chaque e value avc celle que l'utilisateur a fixé
                    if(float(seuil)>float(i)): #si  e value fixée supérieure à celle dans le dictionnaire
                        #on insère uniquement les e value plus petites que le seuil
                        self.tab.insert('','end',values=(self.target_name,self.query_id,i,self.domain_start,self.domain_end,self.tlength,cov))
                        self.tab.grid(row=0, column=0, sticky=N+S+E+W)
                        self.s.grid(row=0, column=1, sticky="ns")


    def get_value(self):
         #pour récupérer ce que l'utilisateur veut insérer dans la base de données

        curItem= self.tab.selection()
        curItems = [(self.tab.item(i)['values']) for i in curItem]
        if (self.var_scan.get())==1 and (self.var_search.get())==0:


            for i in curItems:



                sql5 = """ INSERT INTO Domains (id_target,id_query,e_value,Domain_start,Domain_end,tlength,Coverage,file_origin) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}')""".format(i[1],i[0],i[2],i[3],i[4],i[5],i[6],"hmmscan")
                mr.insertion(sql5)
        elif self.var_scan.get()==0 and self.var_search.get()==2:
            for i in curItems:


                sql5 = """ INSERT INTO Domains (id_target,id_query,e_value,Domain_start,Domain_end,tlength,Coverage,file_origin) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}')""".format(i[0],i[1],i[2],i[3],i[4],i[5],i[6],"hmmsearch")
                mr.insertion(sql5)
        else:
            print("You must select a radiobutton")




app = Parser()
app.window.mainloop()
#Reminder
#hmmsearch Search a protein profile HMM against a protein sequence database.
#hmmscan Search a protein sequence against a protein profile HMM database.
#Here, we show an example of using hmmscan, which, you will see, produces output very much like that
#of hmmsearch.
            
	