<!doctype html>

<html>

<!--
MESBAH Ismahène
KALIC Adeline
http://adeline.kalic.etu.perso.luminy.univ-amu.fr/Partie3.php
scp -P 8012 --- k19019128@sasweb.luminy.univ-amu.fr:
-->

        <head>
                <title>Domain interface of HMM</title>
                <meta http-equiv="Content-Type" content="utf-8">
                <link rel="stylesheet" href="Partie3.css" />


    <script>



        window.onload = function () {

//Better to construct options first and then pass it as a parameter
var options = {
        animationEnabled: true,
        theme: "light2",
        title:{
                text: "Domains of protein"
        },
        axisY2:{

                lineThickness: 0
        },
        toolTip: {
                shared: false
        },
        legend:{
                verticalAlign: "top",
                horizontalAlign: "center"
        },
        data: [
        {
                type: "stackedBar",
                showInLegend: true,
                name: "Domaine",
                axisYType: "secondary",
                color: "#7E8F74",
                dataPoints: [
                        { y: 45, label: "Zn_Tnp_IS1595" },
                        { y: 25, label: "Zot" },
                        { y: 206, label: "Acetyltransf_4" },
            { y: 38, label: "Zn_Tnp_IS1595" },
            { y: 54, label: "Zot" }
                ]
        },
        {
                type: "stackedBar",
                showInLegend: true,
                name: "Protéine",
                axisYType: "secondary",
                color: "#F0D6A7",
                dataPoints: [
                        { y: 137, label: "ZOBELLIA_1386" },
                        { y: 500, label: "ZOBELLIA_2581" },
                        { y: 360, label: "ZOBELLIA_1487" },
            { y: 138, label: "ZOBELLIA_1146" },
            { y: 317, label: "ZOBELLIA_3833" }
                ]
        }
        ]
};

$("#chartContainer").CanvasJSChart(options);
}
        </script>

        </head>

        <body>



            <?php
        include 'cobdd.php'; //pour se connecter à la base de donnée mysql
        ?>

        <h1>Web interface for viewing protein domains</h1>

        <table class="blueTable" >
            <thead>
                <tr align="left">
                    <th>Protein</th>
                    <th>Domain</th>
                    <th>Start</th>
                    <th>End</th>
                    <th>E_value</th>
                    <th>Target length</th>
                    <th>Coverage</th>
                    <th>Original job</th>
                    <th>Link Pfam</th>
                </tr>
            </thead>






            <?php
                    $sql ='SELECT id_target,id_query,Domain_start,Domain_end,e_value,tlength,Coverage,file_origin  FROM Domains ORDER BY id_query ';

            foreach  ($db->query($sql) as $row) {
                echo "<tr>";

                print "<td>".$row['id_target'] . "</td>";
                print "<td>".$row['id_query'] . "</td>";
                print "<td>".$row['Domain_start'] . "</td>";
                print "<td>".$row['Domain_end'] . "</td>";
                print "<td>".$row['e_value'] . "</td>";
                print "<td>".$row['tlength'] . "</td>";
                print "<td>".$row['Coverage'] . "</td>";
                print "<td>".$row['file_origin'] . "</td>";
                print "<td><a href=https://pfam.xfam.org/family/".$row['id_query'].">Pfam</a></td>";

                echo "</tr>";
}

        ?>


    </table>
    <h2>By Ismahene and Adeline</h2>

    <div id="chartContainer" style="height: 300px; width: 100%;"></div>
    <script type="text/javascript" src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>
    </body>
</html>



